const mongoose = require('mongoose');
const UsersGoogle = mongoose.model('usersgoogle');
const Users = mongoose.model('users');
const { mongoURI } = require('../../config/keys');

module.exports = {
  connectDB: () => {
    mongoose.connect(mongoURI);
  },

  disconnectDB: () => {
    mongoose.disconnect();
  },

  findUserGoogle: async param => {
    let user = await UsersGoogle.findOne({ googleId: param });
    return user;
  },
  findUserGoogleById: async id => {
    let user = await UsersGoogle.findById(id);
    return user;
  },
  findUser: async param => {
    let user = await Users.findOne(param);
    return user;
  },
  findUserById: async id => {
    let user = await Users.findById(id);
    return user;
  },
  saveUserGoogle: async user => {
    await new UsersGoogle(user).save();
  },
  saveUser: async user => {
    await new Users(user).save();
  }
};

mongoose.connection.on('open', () => {
  console.log('Mongoose default connection: connected');
});

mongoose.connection.on('disconnected', () => {
  console.log('Mongoose default connection: disconnected');
});

mongoose.connection.on('error', err => {
  console.log('Mongoose default connection: error ' + err);
});

process.on('SIGINT', () => {
  mongoose.connection.close(() => {
    console.log(
      'Mongoose default connection: disconnected through app termination'
    );
    process.exit(0);
  });
});
