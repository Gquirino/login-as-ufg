//Modelo de Usuário sem a conta do Google
const mongoose = require('mongoose');
const { Schema } = mongoose;

const userSchema = new Schema({
    user: String,
    email: String,
    senha: String
});

mongoose.model('users', userSchema);
