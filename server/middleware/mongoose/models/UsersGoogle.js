//Modelo de Usuário com a conta do Google
const mongoose = require('mongoose');
const { Schema } = mongoose;

const userSchema = new Schema({
    googleId: String,
    email: String,
    displayName: String,
    photo: String
});

mongoose.model('usersgoogle', userSchema);
