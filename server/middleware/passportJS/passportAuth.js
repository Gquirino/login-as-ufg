const passport = require('passport');

module.exports = {
  authenticate: (strategy, req, res) => {
    if (strategy == 'google') {
      return passport.authenticate('google', {
        scope: ['profile', 'email']
      });
    } else if (strategy == 'local') {
      return passport.authenticate('local', { session: true }, (err, user) => {
        if (!user) {
          return res.render('../ejs/login', {
            message: 'email/senha incorretos'
          });
        }
        return req.logIn(user, err => {
          res.redirect('/logged');
        });
      });
    }
  },
  googleCallback: () => {
    return passport.authenticate('google');
  }
};
