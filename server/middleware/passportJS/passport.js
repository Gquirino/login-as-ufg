const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const LocalStrategy = require('passport-local').Strategy;
const { googleClientID, googleClientSecret } = require('../../config/keys');
const dao = require('../mongoose/mongoose');

/**
 * Serializando e deserealizando
 * salvando em sessão
 */
passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser(async (id, done) => {
  dao.connectDB();
  let usergoogle = await dao.findUserGoogleById(id);
  let user = await dao.findUserById(id);
  if (usergoogle) {
    done(null, usergoogle);
  } else {
    done(null, user);
  }
  dao.disconnectDB();
});
/**
 * Usando estratégia Google OAuth
 */
passport.use(
  new GoogleStrategy(
    {
      clientID: googleClientID,
      clientSecret: googleClientSecret,
      callbackURL: '/auth/google/callback',
      proxy: true
    },
    async (accessToken, refreshToken, profile, done) => {
      // console.log('AcessTOKEN: ' + accessToken);
      // console.log('RefreshTOKEN: ' + refreshToken);
      // console.log('Profile: ' + JSON.stringify(profile));
      dao.connectDB();
      let existingUser = await dao.findUserGoogle(profile.id);
      dao.disconnectDB();

      if (existingUser) {
        return done(null, existingUser);
      }

      let userGoogle = {
        googleId: profile.id,
        email: profile.emails[0].value,
        displayName: profile.displayName,
        photo: profile.photos[0].value
      };

      dao.connectDB();
      dao.saveUserGoogle(userGoogle);
      dao.disconnectDB();

      return done(null, userGoogle);
    }
  )
);
/**
 * Usando estrategia Local
 */
passport.use(
  new LocalStrategy(
    {
      userNameField: 'email',
      passwordFiled: 'password',
      passReqToCallback: true
    },
    async (req, email, password, done) => {
      dao.connectDB();
      let user = (await dao.findUser({ email: email })) || null;
      dao.disconnectDB();
      if (user == null) return done(null, false);
      if (user.senha != password) return done(null, false);

      done(null, user);
    }
  )
);
