/**
 * Responsável por gerenciar as rotas de autenticação
 */

const {
  authenticate,
  googleCallback
} = require('../middleware/passportJS/passportAuth');

module.exports = app => {
  //inicia autenticação pelo google
  app.get('/auth/google', authenticate('google'));

  //redirecionando o callback para logged
  app.get('/auth/google/callback', googleCallback(), (req, res, next) => {
    res.redirect('/logged?');
  });

  // inicia autenticação com conta local
  app.post('/login', (req, res, next) => {
    authenticate('local', req, res)(req, res, next);
  });
};
