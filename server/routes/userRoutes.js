/**
 * Responsável por gerenciar as rotas de navegação
 */
module.exports = app => {
  //se usuário estiver logado envia a id
  app.get('/', (req, res) => {
    req.user
      ? res.render('../ejs/home', { userId: req.user.id })
      : res.render('../ejs/home', {});
  });

  //renderizando a tela de login
  app.get('/login', (req, res) => {
    res.render('../ejs/login');
  });

  app.get('/logged', (req, res) => {
    res.render('../ejs/logado', {
      userId: req.user.googleId || req.user.id,
      name: req.user.displayName || req.user.user,
      photo: req.user.photo || null,
      email: req.user.email
    });
  });

  //efetuando logout e encaminhando para home
  app.get('/api/logout', (req, res) => {
    req.logout();
    //res.redirect('https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://localhost:5000/auth/google');
    res.redirect('/');
  });

  //rota teste para verificar JSON usuário
  app.get('/api/current_user', (req, res) => {
    req.user ? res.send(req.user) : res.send('Você não está logado');
  });
};
