const cookieSession = require('cookie-session');
const { cookieKey } = require('./keys');
const passport = require('passport');
const bodyParser = require('body-parser');
const expressLayouts = require('express-ejs-layouts');

module.exports = (app, express, path) => {
  app.set('view engine', 'ejs');
  app.use(expressLayouts);
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(express.static(path + '/client/view'));
  app.set('views', path + '/client/view/ejs');
  app.use(
    cookieSession({
      maxAge: 30 * 24 * 60 * 1000,
      keys: [cookieKey]
    })
  );
  app.use(passport.initialize());
  app.use(passport.session({ cookie: { secure: true } }));
};
