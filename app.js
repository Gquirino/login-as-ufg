const express = require('express');
const { port } = require('./server/config/keys');

require('./server/middleware/mongoose/models/UsersGoogle');
require('./server/middleware/mongoose/models/Users');
require('./server/middleware/passportJS/passport');
const app = express();

require('./server/config/app_config')(app, express, __dirname);
require('./server/routes/authRoutes')(app);
require('./server/routes/userRoutes')(app);

app.listen(port, () => {
  console.log('Example listening on port %s ', port);
});
